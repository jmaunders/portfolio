// Fade in all content
setTimeout(function () {
  document.body.style.opacity = 1
}, 500)

// Handle email label movement based on input
var email_input = document.querySelector('#contact input[name="email"]')
email_input.addEventListener('input', function (e) {
  var email_input_label = document.querySelector('#contact label[for="email"]')
  if (email_input.value !== '') {
    email_input_label.style.fontSize = '0.7em';
    // email_input_label.style.left = '7.2em'
    email_input_label.style.top = '0.25em'
  } else {
    email_input_label.style = ''
  }
})