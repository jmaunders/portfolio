$(document).ready(function() {

//Contact Buttons

	function moveToBottom () {
		var windowTop = $(window).scrollTop();
		var divTop = $("#stickyAnchor").offset().top + 35;
		if(windowTop > divTop) {
			$("#stickyContactTop").addClass("hide");
			$("#stickyContactBottom").removeClass("hide");
			$("#stickyAnchor").height($("#stickyContact").outerHeight());
		} else {
			$("#stickyContactTop").removeClass("hide");
			$("#stickyContactBottom").addClass("hide");
			$("#stickyAnchor").height(0);
		}
	}

	$(window).scroll(moveToBottom);

	function fadeOutBtn () {
		var windowBottom = $(window).scrollTop() + $(window).innerHeight();
		var contactFormTop = $("#contact").offset().top;
		if(windowBottom > contactFormTop) {
			$("#stickyContactBottom").toggleClass("hide");
		}
	}

	$(window).scroll(fadeOutBtn);


//Smooth scroll

	$("a[href='#contact']").on("click", function(event) {
		$(".contact-container").fadeOut();
		var target = $(this.getAttribute('href'));
    if( target.length ) {
      event.preventDefault();
      $('html, body').stop().animate({
          scrollTop: target.offset().top
      }, 1000);
    }
    $(".contact-container").delay(1000).fadeIn();
	});

//Icon Animation

	$(window).scroll(function iconAnimation () {
		var windowBottom = $(window).scrollTop() + $(window).innerHeight();
		var iconsTop = $("#logos").offset().top;
		
		if(windowBottom > iconsTop + 150 && !$(".icon").hasClass("done")) {
			$(".icon").css("opacity", 1).addClass("animated zoomIn done");
			setTimeout(function() {
				$(".icon").removeClass("animated zoomIn");
			}, 800);
		}
	});


	//Contact Form Field Focus

	$("form .input-blocks input, form .input-blocks textarea").focusout(function() {
		var text_val = $(this).val();
		if(text_val === "") {
			$(this).removeClass("has-value");
		} else {
			$(this).addClass("has-value");
		}
	})

});